#!/bin/bash
echo '========================='
echo '== Running tests =='
echo '========================='
./vendor/bin/phpunit
if [ $? != 0 ]; then
    exit 1;
fi
