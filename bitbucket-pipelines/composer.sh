#!/bin/bash
echo '========================='
echo '== Installing composer =='
echo '========================='
composer install --no-interaction --no-progress --prefer-dist
if [ $? != 0 ]; then
    exit 1;
fi
