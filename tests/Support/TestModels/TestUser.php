<?php

namespace Way2Web\TwoFactorAuth\Tests\Support\TestModels;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Way2Web\TwoFactorAuth\Traits\HasTwoFactorAuth;

/**
 * Class TestUser.
 */
class TestUser extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, HasTwoFactorAuth;

    /** @var string */
    protected $table = 'users';

    /** @var array */
    protected $fillable = ['email', 'name', 'two_factor_auth_secret'];

    /** @var bool */
    public $timestamps = false;
}
