<?php

namespace Way2Web\TwoFactorAuth\Tests\Api;

use Mockery;
use Way2Web\TwoFactorAuth\Tests\Support\TestModels\TestUser;
use Way2Web\TwoFactorAuth\Tests\TestCase;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * Test Two factor authentication.
 */
class ApiSecretTest extends TestCase
{
    /** @test */
    public function a_secret_can_be_saved_on_the_user_via_the_api()
    {
        $this->login('api');

        $this->instance(TwoFactorAuthService::class, Mockery::mock(TwoFactorAuthService::class, function ($mock) {
            $mock->shouldReceive('check')->andReturn(true);
        }));

        $response = $this->post(route('two-factor-auth.api.secret.store'), [
            'two_factor_auth_secret' => 'MySecret',
            'one_time_password'      => '123456',
        ]);

        $this->assertEquals(TestUser::first()->two_factor_auth_secret, 'MySecret');
        $this->assertEquals($response->getStatusCode(), 200);
    }

    /** @test */
    public function a_secret_can_be_deleted_on_the_user()
    {
        $this->login('api');

        $this->instance(TwoFactorAuthService::class, Mockery::mock(TwoFactorAuthService::class, function ($mock) {
            $mock->shouldReceive('check')->andReturn(true);
        }));

        $response = $this->post(route('two-factor-auth.api.secret.delete'), [
            'one_time_password' => '123456',
        ]);

        $this->assertEquals(TestUser::first()->two_factor_auth_secret, null);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function api_requests_return_json()
    {
        $response = $this->postJson(route('two-factor-auth.api.secret.store'));

        $this->assertJson($response->content());
    }
}
