<?php

namespace Way2Web\TwoFactorAuth\Tests\Api;

use Mockery;
use Way2Web\TwoFactorAuth\Tests\TestCase;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * Test Two factor authentication.
 */
class ApiAuthTest extends TestCase
{
    /** @var string */
    private $secret;

    /**
     * ApiAuthTest constructor.
     *
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->secret = config('two-factor-auth.secret-column-name');
    }

    /** @test */
    public function a_user_can_login_using_an_otp_on_api()
    {
        // a user that has logged in and has a 2fa secret setup receives a code that indicates that 2fa is needed
        $user = $this->login('api');

        // mocker to make sure the otp is accepted
        $this->instance(
            TwoFactorAuthService::class,
            Mockery::mock(TwoFactorAuthService::class, function ($mock) {
                $mock->shouldReceive('check')->andReturn(true);
            })
        );

        $secret = $this->secret;
        $user->$secret = 10000;
        $user->save();

        // upon submission of the correct 2fa code the user is logged in
        $response = $this->post(
            route('two-factor-auth.api.one-time-password.post'),
            ['one_time_password' => '123456']
        );

        $response
            ->assertStatus(200)
            ->assertJson(['data' => ['message' => 'two factor authorized']]);
    }

    /** @test */
    public function a_user_cannot_login_using_the_wrong_otp_on_api()
    {
        // a user that has logged in and has a 2fa secret setup receives a code that indicates that 2fa is needed
        $this->login('api');

        // upon submission of the incorrect 2fa code the user will receive an error
        $response = $this->post(
            route('two-factor-auth.api.one-time-password.post'),
            ['one_time_password' => '000000']
        );

        $response
            ->assertStatus(403)
            ->assertJson([
                'data' => ['error_code' => trans('two-factor-auth::otp-form.message.validation_failed')],
            ]);
    }

    /** @test */
    public function a_user_that_has_not_configured_2fa_gets_a_proper_api_response()
    {
        // a user that has logged in and has a 2fa secret setup receives a code that indicates that 2fa is needed
        $this->login('api');

        // upon submission of the incorrect 2fa code the user will receive an error
        $response = $this->post(
            route('two-factor-auth.api.one-time-password.post'),
            ['one_time_password' => '000000']
        );

        $response
            ->assertStatus(403)
            ->assertJson([
                'data' => ['error_code' => trans('two-factor-auth::otp-form.message.validation_failed')],
            ]);
    }
}
