<?php

namespace Way2Web\TwoFactorAuth\Tests;

use Illuminate\Http\Request;
use Way2Web\TwoFactorAuth\Http\Middleware\Authenticate;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * Test Two factor authentication.
 */
class TwoFactorAuthTest extends TestCase
{
    /** @test */
    public function a_user_is_redirected_if_two_factor_auth_has_not_been_setup()
    {
        $this->login();

        $middleware = new Authenticate(new TwoFactorAuthService());

        $request = Request::create('user', 'GET');

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response->getStatusCode(), 302);
        $this->assertEquals($response->getTargetUrl(), route('two-factor-auth.secret.create'));
    }

    /** @test */
    public function a_user_is_not_redirected_if_two_factor_auth_is_not_enabled()
    {
        config(['two-factor-auth.enabled' => false]);

        $this->login();

        $middleware = new Authenticate(new TwoFactorAuthService());

        $request = Request::create('user', 'GET');

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals(null, $response);
    }

    /** @test */
    public function a_user_is_redirected_if_not_authenticated_with_two_factors()
    {
        $user = $this->login();

        $columnName = config('two-factor-auth.secret-column-name');

        $user->$columnName = 'SomeSecretCode';

        $user->save();

        $middleware = new Authenticate(new TwoFactorAuthService());

        $response = $middleware->handle(request(), function () {
        });

        $this->assertEquals($response->getStatusCode(), 302);
        $this->assertEquals($response->getTargetUrl(), route('two-factor-auth.one-time-password.form'));
    }
}
