<?php

namespace Way2Web\TwoFactorAuth\Tests;

use AddTwoFactorAuthColumns;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Way2Web\TwoFactorAuth\Tests\Support\TestModels\TestUser;

/**
 * Class TestCase.
 */
abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use DatabaseMigrations;

    /**
     * SetUp method.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->setUpConfig();
        $this->setUpDatabase($this->app);
        $this->setUpRoutes();
    }

    /**
     * Login and return a user.
     *
     * @param string|null $guard
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function login(string $guard = null)
    {
        $this->actingAs(TestUser::first(), $guard);

        return auth()->user();
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    protected function setUpDatabase($app)
    {
        $app['db']->connection()->getSchemaBuilder()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
        });

        include_once __DIR__ . '/../database/migrations/add_two_factor_auth_columns.php.stub';

        (new AddTwoFactorAuthColumns())->up();

        TestUser::create(['name' => 'admin', 'email' => 'admin@way2web.nl']);
    }

    /**
     * SetUp some config defaults.
     */
    protected function setUpConfig()
    {
        config([
            'two-factor-auth.enabled'                      => true,
            'two-factor-auth.secret-column-name'           => 'two_factor_auth_secret',
            'two-factor-auth.authenticated-at-column-name' => 'two_factor_authenticated_at',
        ]);
    }

    /**
     * SetUp routes.
     */
    protected function setUpRoutes()
    {
        \Route::get('user', function () {
            return 'content for logged in users';
        })->middleware(['auth']);

        \Route::get('api.user', function () {
            return 'User';
        });

        \Route::middleware('web')
            ->group(__DIR__ . '/../routes/web.php');

        \Route::middleware('api')
            ->group(__DIR__ . '/../routes/api.php');
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('queue.default', 'sync');
        $app['config']->set('app.key', '6rE9Nz59bGRbeMATftriyQjrpF7DcOQm');
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite.database', ':memory:');
    }
}
