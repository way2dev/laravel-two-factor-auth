<?php

namespace Way2Web\TwoFactorAuth\Tests\Web;

use Mockery;
use Way2Web\TwoFactorAuth\Events\TwoFactorAuthenticated;
use Way2Web\TwoFactorAuth\Tests\Support\TestModels\TestUser;
use Way2Web\TwoFactorAuth\Tests\TestCase;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * Test Two factor authentication.
 */
class WebTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();

        $this->instance(TwoFactorAuthService::class, Mockery::mock(TwoFactorAuthService::class, function ($mock) {
            $mock->shouldReceive('check')->andReturn(true);
        }));
    }

    /** @test */
    public function a_secret_can_be_saved_on_the_user()
    {
        $this->login();

        $response = $this->post(route('two-factor-auth.secret.store'), [
            'two_factor_auth_secret' => 'MySecret',
            'one_time_password'      => '123456',
        ]);

        $this->assertEquals(TestUser::first()->two_factor_auth_secret, 'MySecret');
        $this->assertEquals($response->getStatusCode(), 302);
    }

    /** @test */
    public function a_secret_can_be_deleted_on_the_user()
    {
        $this->login();

        $response = $this->post(route('two-factor-auth.secret.delete'), [
            'one_time_password'      => '123456',
        ]);

        $this->assertEquals(TestUser::first()->two_factor_auth_secret, null);
        $this->assertEquals($response->getStatusCode(), 302);
    }

    /** @test */
    public function a_one_time_password_can_be_verified()
    {
        $this->login();

        $response = $this->post(route('two-factor-auth.one-time-password.post'), [
            'one_time_password'      => '123456',
        ]);

        $this->assertEquals($response->getStatusCode(), 302);
    }

    /** @test */
    public function an_event_is_fired_when_a_user_is_two_factor_authenticated()
    {
        $this->expectsEvents(TwoFactorAuthenticated::class);

        $this->login();

        $this->post(route('two-factor-auth.secret.store'), [
            'two_factor_auth_secret' => 'MySecret',
            'one_time_password'      => '123456',
        ]);
    }
}
