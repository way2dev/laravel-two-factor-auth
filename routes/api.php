<?php

Route::group(
    [
        'prefix'     => 'api/two-factor-auth',
        'as'         => 'two-factor-auth.api.',
        'namespace'  => 'Way2Web\TwoFactorAuth\Http\Controllers\Api',
    ],
    function () {
        Route::get('secret', [
            'as'   => 'secret.create',
            'uses' => 'SecretController@create',
        ]);

        Route::post('secret', [
            'as'         => 'secret.store',
            'uses'       => 'SecretController@store',
            'middleware' => ['throttle:50'],
        ]);

        Route::post('secret/delete', [
            'as'   => 'secret.delete',
            'uses' => 'SecretController@delete',
        ]);

        Route::post('one-time-password', [
            'as'         => 'one-time-password.post',
            'uses'       => 'AuthController@verify',
            'middleware' => ['throttle:50'],
        ]);

        Route::get('status', [
            'as' => 'status',
            'uses' => 'AuthController@status',
            'middleware' => ['throttle:50'],
        ]);

        Route::get('status', [
            'as' => 'status',
            'uses' => 'AuthController@status',
            'middleware' => ['throttle:50'],
        ]);
    }
);
