<?php

Route::group(
    [
        'prefix'     => 'two-factor-auth',
        'as'         => 'two-factor-auth.',
        'namespace'  => 'Way2Web\TwoFactorAuth\Http\Controllers\Web',
        'middleware' => ['web'],
    ],
    function () {
        Route::get('secret', [
            'as'   => 'secret.create',
            'uses' => 'SecretController@create',
        ]);

        Route::post('secret', [
            'as'         => 'secret.store',
            'uses'       => 'SecretController@store',
            'middleware' => ['throttle:50'],
        ]);

        Route::get('secret/edit', [
            'as'   => 'secret.edit',
            'uses' => 'SecretController@edit',
        ]);

        Route::post('secret/delete', [
            'as'   => 'secret.delete',
            'uses' => 'SecretController@delete',
        ]);

        Route::get('one-time-password', [
            'as'   => 'one-time-password.form',
            'uses' => 'AuthController@show',
        ]);

        Route::post('one-time-password', [
            'as'         => 'one-time-password.post',
            'uses'       => 'AuthController@verify',
            'middleware' => ['throttle:50'],
        ]);

        Route::get('status', [
            'as'         => 'status',
            'uses'       => 'AuthController@status',
            'middleware' => ['throttle:50'],
        ]);

        Route::get('status', [
            'as'         => 'status',
            'uses'       => 'AuthController@status',
            'middleware' => ['throttle:15'],
        ]);
    }
);
