@extends('two-factor-auth::layout')

@section('title', trans('two-factor-auth::secret-create.title'))

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <header class="text-center">
                <h1>{{ trans('two-factor-auth::secret-create.heading') }}</h1>
            </header>


            <form method="POST" action="{{ route('two-factor-auth.secret.store') }}" id="two-factor-auth-secret-store">
                @csrf


                <div class="text-center">
                    <p>{{ trans('two-factor-auth::secret-create.description', ['secret' => $secret]) }}</p>

                    <div>
                        <input type="hidden" value="{{ $secret }}" name="two_factor_auth_secret">
                        <img src="{{ $qrImageUrl }}" alt="QR Code">
                    </div>

                    <div class="text-center">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="{{ trans('two-factor-auth::secret-create.one-time-password-placeholder') }}" name="one_time_password" autofocus autocomplete="off">
                            @if($errors->has('one_time_password'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('one_time_password') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-block btn-primary">
                    {{ trans('two-factor-auth::secret-create.save') }}
                </button>

            </form>
        </div>
    </div>
@endsection
