@extends('two-factor-auth::layout')

@section('title', trans('two-factor-auth::secret-edit.title'))

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <header class="text-center">
                <h1>{{ trans('two-factor-auth::secret-edit.heading') }}</h1>
            </header>

            <div class="text-center">
                <p>{{ trans('two-factor-auth::secret-edit.description') }}</p>
            </div>

            <form action="{{ route('two-factor-auth.secret.delete') }}" method="post">
                @method('delete')
                @csrf

                @if($errors->has('one_time_password'))
                    <div class="alert alert-danger">
                        {{ $errors->first('one_time_password') }}
                    </div>
                @endif

                <div class="text-center">
                    <div class="form-group">
                        <label for="one_time_password">Code</label>
                        <input id="one_time_password" type="text" name="one_time_password" class="form-control" placeholder="code" autofocus autocomplete="off">
                    </div>
                </div>

                <button type="submit" class="btn btn-block btn-primary">
                    {{ trans('two-factor-auth::secret-edit.action-delete') }}
                </button>
            </form>

        </div>
    </div>
@endsection
