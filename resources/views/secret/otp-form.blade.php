@extends('two-factor-auth::layout')

@section('title', trans('two-factor-auth::otp-form.title'))

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            <header class="text-center">
                <h1>{{ trans('two-factor-auth::otp-form.heading') }}</h1>
                <p>{{ trans('two-factor-auth::otp-form.description') }}<p>
            </header>

            {!! Form::open([
                'route'  => 'two-factor-auth.one-time-password.post',
                'method' => 'post',
                'id'     => 'two-factor-auth-one-time-password-post'
            ]) !!}

            @if($errors->has('one_time_password'))
                <div class="alert alert-danger">
                    {{ $errors->first('one_time_password') }}
                </div>
            @endif

            <div class="text-center">
                <div class="form-group">
                    {{ Form::text('one_time_password', null, [
                        'class' => 'form-control',
                        'placeholder' => 'code',
                        'autofocus',
                        'autocomplete' => 'off'
                    ]) }}
                </div>
            </div>

            <button type="submit" class="btn btn-block btn-primary">
                {{ trans('two-factor-auth::otp-form.save') }}
            </button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
