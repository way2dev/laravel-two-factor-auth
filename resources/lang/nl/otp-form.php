<?php

return [
    'title'       => 'Tweestapsverificatie',
    'heading'     => 'Tweestapsverificatie',
    'description' => 'Gebruik een Authenticater app om je beveiligingscode op te halen',
    'save'        => 'Inloggen',
    'message'     => [
        'validation_failed' => 'De beveiligingscode is niet correct',
        'secret_saved'      => 'Two factor secret saved',
    ],
];
