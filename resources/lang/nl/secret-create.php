<?php

return [
    'title'                         => 'Beveilig je account',
    'heading'                       => 'Beveilig je account',
    'description'                   => 'Registreer je tweestapsverificatie door de barcode hieronder te scannen. Of gebruik de volgende code :secret',
    'one-time-password-placeholder' => 'Code',
    'save'                          => 'Registratie voltooien',
    'message'                       => [
        'stored' => 'De tweestapsverificatie code is succesvol opgeslagen.',
    ],
];
