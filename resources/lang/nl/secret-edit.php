<?php

return [
    'heading'       => 'Reset tweestapsverificatie',
    'description'   => 'Reset het tweestapsverificatie proces voor je account. Let op: hierna zul je opnieuw je account moeten verifiëren met Google Authenticator',
    'action-delete' => 'Reset',
    'message'       => [
        'secret-removed' => 'Tweestapsverificatiecode gereset',
    ],
];
