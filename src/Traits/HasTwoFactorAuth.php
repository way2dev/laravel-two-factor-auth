<?php

namespace Way2Web\TwoFactorAuth\Traits;

use Carbon\Carbon;
use Way2Web\TwoFactorAuth\Events\TwoFactorAuthenticated;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * Trait HasTwoFactorAuth.
 */
trait HasTwoFactorAuth
{
    /**
     * Encrypt the user's two factor auth secret.
     *
     * @param string $value
     */
    public function setTwoFactorAuthSecretAttribute($value)
    {
        if ($value) {
            $value = encrypt($value);
        }

        $secretColumn = config('two-factor-auth.secret-column-name');
        $this->attributes[$secretColumn] = $value;
    }

    /**
     * Decrypt the user's two factor auth secret.
     *
     * @param string $value
     *
     * @return string
     */
    public function getTwoFactorAuthSecretAttribute($value)
    {
        // We check for a value because decrypt on a null value throws an error
        if ($value) {
            return decrypt($value);
        }

        return '';
    }

    /**
     * @param array $values
     *
     * @return array
     */
    protected function getArrayableItems(array $values)
    {
        $secretColumnName = config('two-factor-auth.secret-column-name');

        if (!in_array($secretColumnName, $this->hidden)) {
            $this->hidden[] = $secretColumnName;
        }

        return parent::getArrayableItems($values);
    }

    /**
     * @param string $secret
     *
     * @return bool
     */
    public function saveTwoFactorAuthSecret(string $secret)
    {
        $secretColumnName = config('two-factor-auth.secret-column-name');

        $this->$secretColumnName = $secret;

        return $this->save();
    }

    /**
     * Sets the Authenticated At Attribute to indicate that User has been two-factor authenticated.
     */
    public function setNewTwoFactorAuthenticatedAt()
    {
        $authAttribute = config('two-factor-auth.authenticated-at-column-name');
        $this->$authAttribute = Carbon::now();
        $this->save();

        event(new TwoFactorAuthenticated($this));
    }

    /**
     * Remove the secret to reset the 2fa proces.
     */
    public function resetTwoFactorAuth()
    {
        $secretAttribute = config('two-factor-auth.secret-column-name');
        $this->$secretAttribute = null;

        $authAttribute = config('two-factor-auth.authenticated-at-column-name');
        $this->$authAttribute = null;

        $this->save();
    }

    /**
     * Remove the two factor timestamp.
     */
    public function unsetTwoFactorAuthDate()
    {
        $authAttribute = config('two-factor-auth.authenticated-at-column-name');
        $this->$authAttribute = null;
        $this->save();
    }

    /**
     * @return bool
     */
    public function hasRegisteredTwoFactorAuth()
    {
        if (!TwoFactorAuthService::isEnabled()) {
            return true;
        }

        $secret = $this->getTwoFactorAuthSecret();

        if (!$secret) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getTwoFactorAuthSecret()
    {
        $secretColumnName = config('two-factor-auth.secret-column-name');

        return $this->$secretColumnName;
    }

    /**
     * @return string
     */
    public function isTwoFactorAuthenticated()
    {
        if (!TwoFactorAuthService::isEnabled()) {
            return true;
        }

        $authColumnName = config('two-factor-auth.authenticated-at-column-name');

        return $this->$authColumnName;
    }
}
