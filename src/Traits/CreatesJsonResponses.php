<?php

namespace Way2Web\TwoFactorAuth\Traits;

/**
 * Trait CreatesJsonResponses.
 */
trait CreatesJsonResponses
{
    /**
     * @param array $data
     * @param int   $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonResponse(array $data, $statusCode = 200)
    {
        return response()->json(['data' => $data], $statusCode);
    }

    /**
     * @param string $errorMessage
     * @param int    $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonErrorResponse(string $errorMessage, $statusCode = 403)
    {
        return $this->jsonResponse(['error_code' => $errorMessage], $statusCode);
    }
}
