<?php

namespace Way2Web\TwoFactorAuth;

use Sonata\GoogleAuthenticator\GoogleAuthenticator;
use Sonata\GoogleAuthenticator\GoogleQrUrl;

/**
 * Class TwoFactorAuthService.
 */
class TwoFactorAuthService
{
    /**
     * Method to determine if Two Factor Auth is enabled.
     *
     * @return bool
     */
    public static function isEnabled()
    {
        return (bool) config('two-factor-auth.enabled');
    }

    /**
     * Generates an URL to a QR image based on a Secret and the an email.
     *
     * @param string $email
     * @param string $secret
     *
     * @return string
     */
    public function generateQrUrl(string $email, string $secret)
    {
        return GoogleQrUrl::generate($email, $secret, urlencode(config('app.name')));
    }

    /**
     * Generates a new secret.
     *
     * @return string
     */
    public function generateSecret()
    {
        $authenticator = new GoogleAuthenticator();

        return $authenticator->generateSecret();
    }

    /**
     * Validates the supplied Secret against the supplied One Time Password (code).
     *
     * @param string $secret
     * @param string $code
     *
     * @return bool
     */
    public function check(string $secret, string $code)
    {
        $authenticator = new GoogleAuthenticator();

        return $authenticator->checkCode($secret, $code);
    }
}
