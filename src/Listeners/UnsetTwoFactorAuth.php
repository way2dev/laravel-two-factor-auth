<?php

namespace Way2Web\TwoFactorAuth\Listeners;

/**
 * Class UnsetTwoFactorAuth.
 */
class UnsetTwoFactorAuth
{
    /**
     * Handle the event.
     */
    public function handle()
    {
        $user = $this->getCurrentUser();

        if ($user) {
            $user->unsetTwoFactorAuthDate();
        }
    }

    /**
     * Get the current User based on the request.
     *
     * @return mixed
     */
    private function getCurrentUser()
    {
        if (request()->wantsJson()) {
            return auth()->guard('api')->user();
        }

        return auth()->user();
    }
}
