<?php

namespace Way2Web\TwoFactorAuth\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Way2Web\TwoFactorAuth\Traits\CreatesJsonResponses;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * Authenticate middleware.
 */
class Authenticate
{
    use CreatesJsonResponses;

    /** @var TwoFactorAuthService */
    private $service;

    /**
     * Authenticate constructor.
     *
     * @param TwoFactorAuthService $service
     */
    public function __construct(TwoFactorAuthService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $guard = null)
    {
        if (!$this->service->isEnabled()) {
            return $next($request);
        }

        $user = auth()->user($guard);

        if (!$user->hasRegisteredTwoFactorAuth()) {
            return $this->getRegisterResponse();
        }

        if (!$user->isTwoFactorAuthenticated()) {
            return $this->getAuthenticateResponse();
        }

        return $next($request);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    private function getRegisterResponse()
    {
        if (request()->wantsJson()) {
            return $this->jsonErrorResponse('two_factor_not_registered');
        }

        return redirect()->route('two-factor-auth.secret.create');
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    private function getAuthenticateResponse()
    {
        if (request()->wantsJson()) {
            return $this->jsonErrorResponse('two_factor_unauthorized');
        }

        return redirect()->route('two-factor-auth.one-time-password.form');
    }
}
