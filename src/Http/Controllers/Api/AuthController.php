<?php

namespace Way2Web\TwoFactorAuth\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Way2Web\TwoFactorAuth\Http\Requests\OneTimePasswordVerifyRequest;
use Way2Web\TwoFactorAuth\Traits\CreatesJsonResponses;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * The TwoFactorAuthController controller.
 */
class AuthController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, CreatesJsonResponses;

    /**
     * Verifies the one time password against the secret saved on the user model.
     *
     * @param OneTimePasswordVerifyRequest $request
     * @param TwoFactorAuthService         $service
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(OneTimePasswordVerifyRequest $request, TwoFactorAuthService $service)
    {
        $user = auth()->guard('api')->user();

        $otp = $request->get('one_time_password');

        if (! $service->check($user->getTwoFactorAuthSecret(), $otp)) {
            return $this->jsonErrorResponse(trans('two-factor-auth::otp-form.message.validation_failed'));
        }

        $user->setNewTwoFactorAuthenticatedAt();

        return $this->jsonResponse(['message' => 'two factor authorized']);
    }

    /**
     * API function to check the current 2fa status of the user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status()
    {
        $user = auth()->guard('api')->user();

        if (! $user->hasRegisteredTwoFactorAuth()) {
            return $this->jsonErrorResponse('two_factor_not_registered');
        }

        if (! $user->isTwoFactorAuthenticated()) {
            return $this->jsonErrorResponse('two_factor_unauthorized');
        }

        return $this->jsonResponse(['message' => 'authenticated']);
    }
}
