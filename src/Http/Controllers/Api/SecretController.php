<?php

namespace Way2Web\TwoFactorAuth\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Way2Web\TwoFactorAuth\Http\Requests\StoreSecretRequest;
use Way2Web\TwoFactorAuth\Traits\CreatesJsonResponses;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * The SecretController controller. Used to create / store / delete the 2fa secret on the User Model.
 */
class SecretController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, CreatesJsonResponses;

    /** @var TwoFactorAuthService */
    private $service;

    /**
     * TwoFactorAuthController constructor.
     *
     * @param TwoFactorAuthService $service
     */
    public function __construct(TwoFactorAuthService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $email = auth()->guard('api')->user()->email;

        $secret = $this->service->generateSecret();
        $qrUrl = $this->service->generateQrUrl($email, $secret);

        return $this->jsonResponse([
            'secret'       => $secret,
            'qr_image_url' => $qrUrl,
        ]);
    }

    /**
     * Saves the secret to the user model.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSecretRequest $request)
    {
        $secret = $request->get('two_factor_auth_secret');
        $code = $request->get('one_time_password');

        if (! $this->service->check($secret, $code)) {
            return $this->jsonErrorResponse(
                trans('two-factor-auth::otp-form.message.validation_failed'),
                422
            );
        }

        $user = auth()->guard('api')->user();
        $user->saveTwoFactorAuthSecret($secret);
        $user->setNewTwoFactorAuthenticatedAt();

        return $this->jsonResponse(['message' => trans('two-factor-auth::otp-form.message.secret_saved')]);
    }

    /**
     * Removes the secret from the user model and thus resets the two factor auth process.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'one_time_password' => 'required|string|max:6',
        ]);

        $code = $request->get('one_time_password');

        $user = auth()->guard('api')->user();

        if (! $this->service->check($user->getTwoFactorAuthSecret(), $code)) {
            return $this->jsonErrorResponse(
                trans('two-factor-auth::otp-form.message.validation_failed'),
                422
            );
        }

        $user->resetTwoFactorAuth();

        return $this->jsonResponse([
            'message' => trans('two-factor-auth::secret-edit.message.secret-removed'),
        ]);
    }
}
