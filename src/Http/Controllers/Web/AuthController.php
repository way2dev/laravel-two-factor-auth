<?php

namespace Way2Web\TwoFactorAuth\Http\Controllers\Web;

use Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use View;
use Way2Web\TwoFactorAuth\Http\Requests\OneTimePasswordVerifyRequest;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * The TwoFactorAuthController controller.
 */
class AuthController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Shows the one time password form.
     *
     * @return View
     */
    public function show()
    {
        if (auth()->user()->hasRegisteredTwoFactorAuth()) {
            return view('two-factor-auth::secret.otp-form');
        }

        return redirect()->route('two-factor-auth.secret.create');
    }

    /**
     * Verifies the one time password against the secret saved on the user model.
     *
     * @param OneTimePasswordVerifyRequest $request
     * @param TwoFactorAuthService         $service
     *
     * @return RedirectResponse
     */
    public function verify(OneTimePasswordVerifyRequest $request, TwoFactorAuthService $service)
    {
        $user = auth()->user();

        $otp = $request->get('one_time_password');

        if (!$service->check($user->getTwoFactorAuthSecret(), $otp)) {
            return back()->withErrors([
                'one_time_password' => trans('two-factor-auth::otp-form.message.validation_failed'),
            ]);
        }

        $user->setNewTwoFactorAuthenticatedAt();

        return redirect('/');
    }
}
