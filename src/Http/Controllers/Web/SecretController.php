<?php

namespace Way2Web\TwoFactorAuth\Http\Controllers\Web;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Way2Web\TwoFactorAuth\Http\Requests\OneTimePasswordVerifyRequest;
use Way2Web\TwoFactorAuth\Http\Requests\StoreSecretRequest;
use Way2Web\TwoFactorAuth\TwoFactorAuthService;

/**
 * The SecretController controller. Used to create / store / delete the 2fa secret on the User Model.
 */
class SecretController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var TwoFactorAuthService */
    private $service;

    /**
     * TwoFactorAuthController constructor.
     *
     * @param TwoFactorAuthService $service
     */
    public function __construct(TwoFactorAuthService $service)
    {
        $this->service = $service;
    }

    /**
     * Secret create method.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function create()
    {
        $secret = $this->service->generateSecret();
        $qrUrl = $this->service->generateQrUrl(auth()->user()->email, $secret);

        return view('two-factor-auth::secret.create', [
            'secret'     => $secret,
            'qrImageUrl' => $qrUrl,
        ]);
    }

    /**
     * Saves the secret to the user model.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(StoreSecretRequest $request)
    {
        $secret = $request->get('two_factor_auth_secret');
        $code = $request->get('one_time_password');

        if (!$this->service->check($secret, $code)) {
            return back()->withErrors([
                'one_time_password' => trans('two-factor-auth::otp-form.message.validation_failed'),
            ]);
        }

        $user = auth()->user();
        $user->saveTwoFactorAuthSecret($secret);
        $user->setNewTwoFactorAuthenticatedAt();

        $request->session()->flash('two-factor-auth', trans('two-factor-auth::secret-create.message.stored'));

        // Todo: add configurable route to return to -> WAYITD-644
        return redirect()->to('/');
    }

    /**
     * Shows the form to reset the secret.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('two-factor-auth::secret.edit');
    }

    /**
     * Removes the secret from the user model and thus resets the two factor auth process.
     *
     * @param OneTimePasswordVerifyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|RedirectResponse
     */
    public function delete(OneTimePasswordVerifyRequest $request)
    {
        $code = $request->get('one_time_password');

        $user = auth()->user();

        if (!$this->service->check($user->getTwoFactorAuthSecret(), $code)) {
            return back()->withErrors([
                'one_time_password' => trans('two-factor-auth::otp-form.message.validation_failed'),
            ]);
        }

        $user->resetTwoFactorAuth();

        $request->session()->flash('two-factor-auth', trans('two-factor-auth::secret-edit.message.secret-removed'));

        return redirect()->to('/');
    }
}
