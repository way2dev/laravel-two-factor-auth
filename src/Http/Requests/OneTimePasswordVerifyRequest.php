<?php

namespace Way2Web\TwoFactorAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request for verifying the one time password agains the Secret stored on the User.
 */
class OneTimePasswordVerifyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the verify request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'one_time_password' => 'required|string|max:6',
        ];
    }
}
