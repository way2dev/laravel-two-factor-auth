<?php

namespace Way2Web\TwoFactorAuth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request for storing verifying the one time password and storing the Secret on the User.
 */
class StoreSecretRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the store request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'two_factor_auth_secret' => 'required|string',
            'one_time_password'      => 'required|string|max:6',
        ];
    }
}
