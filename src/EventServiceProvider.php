<?php

namespace Way2Web\TwoFactorAuth;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider.
 */
class EventServiceProvider extends ServiceProvider
{
    /** @var array */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'Way2Web\TwoFactorAuth\Listeners\UnsetTwoFactorAuth',
        ],
    ];
}
