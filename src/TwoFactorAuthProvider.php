<?php

namespace Way2Web\TwoFactorAuth;

use Illuminate\Support\ServiceProvider;

/**
 * Class TwoFactorAuthProvider.
 */
class TwoFactorAuthProvider extends ServiceProvider
{
    /**
     * Bootstrap the services.
     */
    public function boot()
    {
        $this->loadRoutes();

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'two-factor-auth');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'two-factor-auth');

        $this->publishes([
            __DIR__ . '/../config/two-factor-auth.php' => config_path('two-factor-auth.php'),
        ], 'config');

        $this->publishMigration();

        $this->publishes([
            __DIR__ . '/../resources/views' => base_path('resources/views/vendor/two-factor-auth'),
        ]);
    }

    /**
     * Load routes for web and/or api.
     */
    private function loadRoutes()
    {
        if (config('two-factor-auth.web')) {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        }

        if (config('two-factor-auth.api')) {
            $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        }
    }

    /**
     * Generate filename and publish migration.
     */
    private function publishMigration()
    {
        $migrationFileName = database_path(
            'migrations/' . date('Y_m_d_His', time()) . '_add_two_factor_auth_columns.php'
        );

        $this->publishes([
            __DIR__ . '/../database/migrations/add_two_factor_auth_columns.php.stub' => $migrationFileName,
        ], 'migrations');
    }

    /**
     * Register services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/two-factor-auth.php', 'two-factor-auth');
        $this->app->register(EventServiceProvider::class);
        $this->app->singleton(TwoFactorAuthService::class, function () {
            return new TwoFactorAuthService();
        });
    }
}
