<?php

namespace Way2Web\TwoFactorAuth\Events;

use Illuminate\Queue\SerializesModels;

class TwoFactorAuthenticated
{
    use SerializesModels;

    /**
     * The authenticated user.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
}
