<?php

return [
    /*
     | Setting to enable or disable the checking of Two Factor Authentication.
     */
    'enabled' => env('2FA_ENABLED', true),

    /*
     | Setting to enable the web routes for Two Factor Authentication.
     | These work with default Blade views which can be overridden by your custom views.
     */
    'web' => true,

    /*
     | Setting to enable the API routes which return JSON responses.
     */
    'api' => true,

    /*
     | If you wish to set custom column names for the Two Factor Auth attributes you can change them here.
     */
    'secret-column-name' => 'two_factor_auth_secret',
    'authenticated-at-column-name' => 'two_factor_authenticated_at',
];
