# Way2Web Two Factor Authentication with Google Authenticator
This package supplies the tools for integrating 2FA with Google Authenticator for Laravel Projects.

## Installation
Require the package in your project adding the following repository to you composer.json:
```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/way2dev/laravel-two-factor-auth.git"
    }
]
```
Then you can run:
`composer require way2web/two-factor-auth 0.1.*`

Publish the migration, config and views:

`artisan vendor:publish --provider="Way2Web\TwoFactorAuth\TwoFactorAuthProvider"`
## Integration
The User model needs the following trait:
```php
use Way2Web\TwoFactorAuth\Traits\HasTwoFactorAuth;

class User extends Authenticatable
{
    use HasTwoFactorAuth;
}
```

Next, include the `\Way2Web\TwoFactorAuth\Http\Middleware\Authenticate::class` middleware in a middleware group. (`app/Htpp/Kernel.php`) 
```php
protected $middlewareGroups = [
    'web' => [
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ],

    'api' => [
        'throttle:60,1',
        'bindings',
    ],

    'two-factor-auth' => [
        \App\Http\Middleware\Authenticate::class,
        \Way2Web\TwoFactorAuth\Http\Middleware\Authenticate::class, // <-- add this middleware
    ]
];
```

Now you can use this middleware group as middleware on a route (or controller), e.g.:
```php
<?php

Route::group(
    [
        'prefix'     => 'dashboard',
        'as'         => 'dashboard.',
        'middleware' => ['two-factor-auth', 'verified'],
    ],
    function () {
        Route::get('/index', [
            'as'   => 'index',
            'uses' => 'DashboardController@index',
        ]);

        Route::post('/table', [
            'as'   => 'table',
            'uses' => 'DashboardController@table',
        ]);
    }
);
```

## Web and API routes
By default both the Web and API routes are enabled. The Web routes return blade views.
The API routes return JSON responses. You can disable either Web or API by editing the config.

```php
<?php

return [
    // Disables the web routes
    'web' => false,
    // Disables the api routes
    'api' => false,

    //...
];
```

## Environment setting to enable/disable
It is possible to quickly disable the Two Factor Auth checks via the config or environment variables.
```php
// add this to your .env file to disable 2fa checking.

2FA_ENABLED=false
```

## Available methods
The trait on the User Model provides some methods to check the Two Factor Auth status.
There is a method to check whether the User has registerd a Secret:

`$user->hasRegisteredTwoFactorAuth()`

There is a method to check whether the User has currently authenticated with the One Time Password:

`$user->isTwoFactorAuthenticated()`

There is a method to unauthorize a User (this will require the User to submit a new One Time Password):

`$user->unsetTwoFactorAuthDate()`

And there is a method to reset the entire Two Factor Authentication setup:

`$user->resetTwoFactorAuth()`

# Contributing
Any Way2Web team(member) can submit a PR to this package. Only Lead Developers and two designated responsible 
team members can approve and merge these PR's. For a PR to be merged atleast one of the two designated team members 
should have approved. The designated team members should have a sense of the direction this package is going towards 
and can detect breaking changes more easily because they know of the previous changes made to the code.

### Designated team members
- Ian Scheele <ian.scheele@way2web.nl>
- Arlon Antonius <arlon.antonius@way2web.nl>
